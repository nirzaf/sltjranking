<?php
session_start();
error_reporting(0);
$error;
$eventID;
$evenName;
$Points;
$TotalPoints;
include('admin/includes/config.php');
if (strlen($_SESSION['login']) == 0) {
    header('location:index.php');
} else {

    if (isset($_POST["upload"])) {
        $eventID = $_POST['event'];
        $abc = "SELECT * FROM  tblauthors Where id=:eventID";
        $que = $dbh->prepare($abc);
        $que -> bindParam(':eventID',$eventID, PDO::PARAM_STR);
        $que->execute();
        if($que)
        {
            try
            {
                $res = $que->fetchAll(PDO::FETCH_OBJ);
                if ($que->rowCount() > 0) 
                    {
                        foreach ($res as $re)
                        {
                            $eventName = $re->AuthorName;
                            $Points = $re->Points;
                        }
                    }
            }
            catch(exception $e)
            {
            echo $e;
            }
        }
        else
        {
            echo "<script>alert('Error')</script>";
        }

		$filename = $_FILES["image"]["name"]; // Get the name of the file (including file extension).
		$filename1 = $_FILES["image1"]["name"]; // Get the name of the file (including file extension).
		if($filename != null || $filename1!= null){
		$allowed_filetypes = array('.jpg','.gif','.bmp','.png'); // These will be the types of file that will pass the validation.
		$max_filesize = 10485760; // Maximum filesize in BYTES (currently 10.0 MB).
		$upload_path = 'admin/images/'; // The place the files will be uploaded to (currently a 'files' directory).		
		$ext = substr($filename, strpos($filename,'.'), strlen($filename)-1); // Get the extension from the filename.
		$ext1 = substr($filename, strpos($filename,'.'), strlen($filename)-1); // Get the extension from the filename.

		// Now check the filesize, if it is too large then DIE and inform the user.
		if($filename != null && filesize($_FILES["image"]["tmp_name"]) > $max_filesize)
			die('The Image 1 you attempted to upload is too large.');

		// Now check the filesize, if it is too large then DIE and inform the user.
		if($filename1 != null && filesize($_FILES["image1"]["tmp_name"]) > $max_filesize)
			die('The Image 1 you attempted to upload is too large.');

		// Check if we can upload to the specified path, if not DIE and inform the user.
		if(!is_writable($upload_path))
			die('You cannot upload to the specified directory, please CHMOD it to 777.');
			
		// Upload the file to your specified path.
		if($filename != null)
			$filename = rand(100,1000000).time().$ext; // this will give the file current time so avoid files having the same name
		move_uploaded_file($_FILES["image"]["tmp_name"],$upload_path . $filename);
		if($filename1 != null)
		$filename1 = rand(100,1000000).time().$ext;
		move_uploaded_file($_FILES["image1"]["tmp_name"],$upload_path . $filename1);
		}
        $Count = $_POST['count'];
        $Type= $_SESSION['Type'];
        $BranchName = $_SESSION['Branch'];
        $DistrictHead = $_SESSION['DistrictHead'];
        $District = $_SESSION['District'];
        $Date = $_POST['eventDate'];
        $Description = $_POST['description'];
        $DoneBy = $_POST['doneby'];
        $Crowd = $_POST['crowd'];
        $Status  = 0;
        $TotalPoints = $Points * $Count;
        try
        {
			try
			{
			$sql = "INSERT INTO tblstudents(EventID, Event_Name, Branch_Name, DistrictHead, District, Done_By, Status, Count, Branch_Type, EventDate, Points, Crowd, image1, image2) VALUES(:ei,:en,:bn,:dh,:dn,:db,:st,:co,:bt,:ed,:po,:cr,:im1,:im2)";
			$query = $dbh->prepare($sql);
			$query->bindParam(':ei', $eventID, PDO::PARAM_INT);
			$query->bindParam(':en', $eventName, PDO::PARAM_STR);
			$query->bindParam(':bn', $BranchName, PDO::PARAM_STR);
			$query->bindParam(':dh', $DistrictHead, PDO::PARAM_STR);
			$query->bindParam(':dn', $District, PDO::PARAM_STR);
			$query->bindParam(':db', $DoneBy, PDO::PARAM_STR);
			$query->bindParam(':st', $Status , PDO::PARAM_INT);
			$query->bindParam(':co', $Count, PDO::PARAM_INT);
			$query->bindParam(':bt', $Type, PDO::PARAM_INT);
			$query->bindParam(':ed', $Date, PDO::PARAM_STR);
			$query->bindParam(':po', $TotalPoints, PDO::PARAM_STR);
			$query->bindParam(':cr', $Crowd, PDO::PARAM_STR);
			$query->bindParam(':im1', $filename, PDO::PARAM_STR);
			$query->bindParam(':im2', $filename1, PDO::PARAM_STR);
			$query->execute();     
			$lastInsertId = $dbh->lastInsertId();
				if($lastInsertId)
				{              
					$_SESSION['msg'] = "Event Listed successfully";
					header('location:manage-events.php');
				}else
				{
					echo "<script>alert('Oops! Please try again')</script>";
				}      
			}
			catch(PDOException $db) 
			{
				$_SESSION['error'] = $db." Something went wrong. Please try again";
				header('location:manage-events.php');
			}
		}
		catch(PDOException $ex)
			{
				$error = $ex;
				throw $ex; 
			}
		}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<style>
	.form-group {
		margin-bottom: 15px;
	}
	.form-control {
	 display: block;
	 width: 100%;
	 height: 34px;
	 padding: 6px 12px;
	 font-size: 14px;
	 line-height: 1.42857143;
	 color: #555;
	 background-color: #fff;
	 background-image: none;
	 border: 1px solid #ccc;
	 border-radius: 4px;
	 -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
			 box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	 -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
		  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
			 transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
	}
      </style>

    <title>SLTJ Ranking Management System | Add Event Details</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME STYLE  -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <script>
            function validate(evt) {
                var theEvent = evt || window.event;

                // Handle paste
                if (theEvent.type === 'paste') {
                    key = event.clipboardData.getData('text/plain');
                } else {
                    // Handle key press
                    var key = theEvent.keyCode || theEvent.which;
                    key = String.fromCharCode(key);
                }
                var regex = /[0-9]|\./;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();
                }
            }
        </script>
</head>
<body>
    <!------MENU SECTION START-->
    <?php include('includes/header.php'); ?>
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Event Info
                        </div>
                        <div class="panel-body">
                            <form role="form" method="post" action="event.php" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Event Name<span style="color:red;">*</span></label>
                                    <select class="form-control" name="event" required="required">
                                        <option value=""> Select Event</option>
                                        <?php
									$sql = "SELECT * FROM  tblauthors";
									$query = $dbh->prepare($sql);
									$query->execute();
									$results = $query->fetchAll(PDO::FETCH_OBJ);
									$cnt = 1;
									if ($query->rowCount() > 0) {
										foreach ($results as $result) { ?>
											<option value="<?php echo htmlentities($result->id); ?>">
												<?php echo htmlentities($result->AuthorName); ?></option>
											<?php }
									}?>
                                    </select>
                                    <label>Event Count<span style="color:red;">*</span></label>
                                    <input class="form-control" type="text" onkeypress='validate(event)' name="count"
                                        autocomplete="off" required />
                                    <label>Event Date<span style="color:red;">*</span></label>
                                    <input class="form-control" type="date" name="eventDate" autocomplete="off" required />                                       
                                    <label>Description</label>
                                    <input class="form-control" type="text" name="description" placeholder="Optional" autocomplete="off" />
                                    <label>Done by<span style="color:red;">*</span></label>
                                    <input class="form-control" type="text" name="doneby" required="required" autocomplete="off" />
                                    <label>Estimated Crowd<span style="color:red;">*</span></label>
                                    <input class="form-control" placeholder="Optional" type="text" name="crowd" onkeypress='validate(event)' autocomplete="off"/>                                
                                    <label ><span style="color:red;">Event Image 1</span></label>
                                    <input class="form-control" type="file" name='image' id="image" />                                    
                                    <label ><span style="color:red;">Event Image 2</span></label>
                                    <input class="form-control" type="file" name='image1' id="image1"/>                                    
                                    <div class="form-group"></div>                                    
                                    <div style="align-content:center"> <button type="submit" name="upload" class="btn btn-primary">Add Event</button> </div>
									<div class="text-center"> &copy; SLTJ Ranking Management System </div> 
									<div class="text-center"><a href="https://nirzaf.github.io/"> Designed by : Fazrin</a> </div>
								</div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
        <!-- CONTENT-WRAPPER SECTION END-->
        <!-- CORE JQUERY  -->
        <script src="assets/js/jquery-1.10.2.js"></script>
        <!-- BOOTSTRAP SCRIPTS  -->
        <script src="assets/js/bootstrap.js"></script>
        <!-- CUSTOM SCRIPTS  -->
        <script src="assets/js/custom.js"></script>
</body>
</html>
<?php }?>